#include <stdio.h>
#include <stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <signal.h>
int main() {
	pid_t pids[10],pid;
	int state;
  int i;
  for(i=0;i<10;i++){
    pids[i] = fork(); 
    if(pids[i]<0){
      return -1;
    }
    else if(pids[i] == 0){
      exit(100+i);
    }
    else{
      printf("parent %d, child %d is Created\n", getpid(), pids[i]);
    }
  }
  for(i=0;i<10;i++){
    printf("sleep 1sec....  ");
    sleep(1);
    kill(pids[i],15);
    printf("child %d has benn killed\n",pids[i]);
  }

  return 0;
}